#!/usr/bin/env bash
ANDROID_NDK_ROOT=/Users/funplus/android-ndk-r14b
#ANDROID_NDK_ROOT=/Users/funplus/Library/Android/sdk/ndk-bundle
PREBUILT=$ANDROID_NDK_ROOT/toolchains/arm-linux-androideabi-4.9/prebuilt/darwin-x86_64
PLATFORM=$ANDROID_NDK_ROOT/platforms/android-14/arch-arm
CROSS_PREFIX=$PREBUILT/bin/arm-linux-androideabi-

#声明CFLAGS、LDFLAGS，目的：在编译时找到正确的头文件；在链接时链接到正确的库文件。-march=armv7-a让编译器知道编译的目标平台是armv7-a
export LDFLAGS="-L$PLATFORM/usr/lib -L$PREBUILT/arm-linux-androideabi/lib -march=armv7-a -lm"
export CFLAGS="-I$PLATFORM/usr/include -march=armv7-a -mfloat-abi=softfp -mfpu=vfp -ffast-math -O2"


export CPPFLAGS="$CFLAGS"
export CFLAGS="$CFLAGS"
export CXXFLAGS="$CFLAGS"
export LDFLAGS="$LDFLAGS"


#AS:将汇编文件生成目标文件（汇编文件使用的是指令助记符，AS将它翻译成机器码）
export AS=${CROSS_PREFIX}as

#LD:链接器，为目标代码分配地址空间，将多个目标文件链接成一个库或者可执行文件 
export LD="${CROSS_PREFIX}ld"

#链接
export CXX="${CROSS_PREFIX}g++ --sysroot=${PLATFORM}"

#编译器
export CC="${CROSS_PREFIX}gcc --sysroot=${PLATFORM} -march=armv7-a"

export CPP=${CROSS_PREFIX}cpp

#nm:查看静态文件中的符号表
export NM=${CROSS_PREFIX}gcc-nm

#消除生成的库中的源码
export STRIP=${CROSS_PREFIX}strip

#
export RANLIB=${CROSS_PREFIX}ranlib

#打包器，从一个库中删除或增加目标代码模块
export AR=${CROSS_PREFIX}gcc-ar

#configure
./configure --host=arm-linux \
--disable-shared \
--disable-frontend \
--enable-static \
--prefix=$(pwd)/armv7a